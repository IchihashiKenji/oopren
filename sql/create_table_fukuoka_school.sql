USE fukuoka_kadai;

DROP TABLE IF EXISTS fukuoka_school;

CREATE TABLE fukuoka_school(
    category_id  INT,
    content_id   INT  NOT NULL PRIMARY KEY,
    school_name  VARCHAR(255)  NOT NULL,
    ku_id        INT NOT NULL,
    post        VARCHAR(12) DEFAULT NULL,
    address     VARCHAR(255) ,
    lat         DOUBLE(8, 6),
    lng         DOUBLE(9, 6),
    tel         VARCHAR (16),
    fax         VARCHAR(16),
    mail        VARCHAR(255) DEFAULT NULL,
    url         VARCHAR(255) DEFAULT NULL
);

