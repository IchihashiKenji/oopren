<?php
require_once __DIR__ . '/class/WriteFile.class.php';
require_once __DIR__ . '/class/GetData.class.php';
require_once __DIR__ . '/class/GetDataTypeTxt.class.php';
require_once __DIR__ . '/class/GetDataTypeCsv.class.php';
require_once __DIR__ . '/class/GetDataTypeJson.class.php';

$resourceInfo = array(
    // データベース名
    'fukuoka_kadai',
    // テーブル名
    'fukuoka_school',
    // ファイル名
    'fukuoka_file'
);

echo "変換する拡張子に対応する数字を入力してください\r\n1:TXTファイル\r\n2:CSVファイル\r\n3:JSONファイル\r\n";
fscanf(STDIN, '%d', $fileExt);
$fileTypeObject = getFileTypeObject($resourceInfo, $fileExt);

echo "出力方法を指定してください".PHP_EOL."1:ファイル".PHP_EOL."2:CLI".PHP_EOL;
fscanf(STDIN, '%d', $fileOutNum);
switch ($fileOutNum) {
    case 1:
        WriteFile::writeDataToFile($fileTypeObject);
        echo $fileTypeObject->fileName . "が作成されました。".PHP_EOL;
        exit();
    case 2:
        writeStdin($fileTypeObject);
        break;
    default:
        echo "入力が不適切です。もう一度実行してください！".PHP_EOL;
        exit();
}

function getFileTypeObject($resourceInfo, $fileExt) {
    // ポリモーフィズム
    switch ($fileExt) {
        case 1:
            $obj = new GetDataTypeTxt($resourceInfo);
            break;
        case 2:
            $obj = new GetDataTypeCsv($resourceInfo);
            break;
        case 3:
            $obj = new GetDataTypeJson($resourceInfo);
            break;
        default :
            echo "入力が不適切です。もう一度実行してください！".PHP_EOL;
            exit();
    }
    return $obj;
}

function writeStdin($obj) {
    foreach ((array) $obj->getData() as $str) {
        echo $str;
    }
    echo PHP_EOL;
    exit();
}
