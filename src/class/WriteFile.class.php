<?php

class WriteFile {
// 書き込みメソッド
    function writeDataToFile($obj) {
        if (! file_exists($obj->fileName)) {
            touch($obj->fileName);
        }
        if (file_exists($obj->fileName)) {
            $fp = fopen($obj->fileName, "w");

            foreach ((array)$obj->getData() as $record) {
                fwrite($fp, $record);
            }
            fclose($fp);
        }
    }
}
