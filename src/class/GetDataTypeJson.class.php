<?php
require_once __DIR__ . '/Connect.class.php';
require_once __DIR__ . '/GetData.class.php';

class GetDataTypeJson extends GetData {

    public $fileName;

    public function __construct($resourceInfo) {
        parent::__construct($resourceInfo);
        $this->fileName = $this->fileName . '.json';
    }
    // getDb　オーバーライド
    public function getData() {
        foreach ($this->records as $record) {
            foreach ($record as $key => $reco) {
                if (! is_int($key)) {
                    continue;
                }
                $data[] = json_encode($reco);
            }
        }
        return $data;
    }
}