<?php
require_once __DIR__ . '/Connect.class.php';
define("OUTPUT_PATH", __DIR__.'/../../output_file/');

class GetData {

    protected $records = array();
    protected $dbName;
    protected $tableName;
    protected $fileName;

    public function __construct($resourceInfo) {
        $this->dbName = $resourceInfo[0];
        $this->tableName = $resourceInfo[1];
        $this->fileName = OUTPUT_PATH . $resourceInfo[2];
        $connect = new Connect($this->dbName);
        $db = $connect->getDb();
        $fetchAllSql = 'select * from ' . $this->tableName;

        $rows = $db->query($fetchAllSql);

        foreach ($rows as $row) {
            $this->records[] = $row;
        }
    }
}

