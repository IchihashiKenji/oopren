<?php
require_once __DIR__ . '/Connect.class.php';
require_once __DIR__ . '/GetData.class.php';

class GetDataTypeCsv extends GetData {

    public $fileName;

    public function __construct($resourceInfo) {
        parent::__construct($resourceInfo);
        $this->fileName = $this->fileName . '.csv';
    }
    // getDb　オーバーライド
    public final function getData() {
        foreach ($this->records as $record) {
            foreach ($record as $key => $reco) {
                if (! is_int($key)) {
                    continue;
                }
                $reco = mb_convert_encoding($reco, 'SJIS-win', 'UTF-8');
                if ($key == 11) {
                    $reco .= "\r\n";
                } else {
                    $reco .= ",";
                }
                $data[] = $reco;
            }
        }
        return $data;
    }
}