<?php

class Connect {

    private $dsn;
    protected $usr = 'root';
    protected $password = '';

    public function __construct($dbname) {
        $this->dsn = sprintf
            ('mysql:dbname=%s;host=localhost;charset=utf8;',$dbname);
    }

    public function getDb() {
        try {
            $db = new PDO($this->dsn, $this->usr, $this->password);
        } catch (PDOException $e) {
            die("接続エラー:({$e->getMessage()}");
        }
        return $db;
    }

    public function exitDb() {
        $db = null;
        exit;
    }


}