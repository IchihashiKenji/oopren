<?php
require_once __DIR__.'/src/CreateSql.php';
require_once __DIR__.'/../../src/class/Connect.class.php';

/*CSVをインポートしてSQLを生成、実行*/
/*データベースへインサートするファイル*/

//CSVとSQLの場所
$csvPath =  __DIR__.'/../../csv/D01.csv';
$sqlPath =  __DIR__.'/../../sql/create_table_fukuoka_school.sql';
$tableName = 'fukuoka_school';


//テーブル定義SQL
$createTableSql = readSqlFile($sqlPath);

//CSV読み込み
$csvAncClmCount = readCsvFile($csvPath);
$csv = $csvAncClmCount[0];
$clmCount = $csvAncClmCount[1];

//挿入SQL
$insertDataSqlAndClmName = createInsertSql($csv, $tableName, $clmCount);

$insertDataSql = $insertDataSqlAndClmName[0]; //insert文
$clmName =$insertDataSqlAndClmName[1]; //カラム名
var_dump($clmName);
var_dump($insertDataSql);

//DB接続ここから-------
$createDb = new Connect('fukuoka_kadai');
$db = $createDb->getDb();

//SQL実行
var_dump($db->query($createTableSql));
var_dump($db->query($insertDataSql));

$db = null;
//DB切断ここまで-------
