<?php

function readSqlFile($sqlPath) {
    return file_get_contents($sqlPath);
}

function readCsvFile($csvPath) {
    // csvFileを読み込み中身を配列で返す
    $fp = fopen($csvPath, "r");
    $csv = array();
    while (($data = fgetcsv($fp, 0, ',')) != false) {
        $csv[] = $data;
    }
    fclose($fp);
    mb_convert_variables('UTF-8', 'SJIS-win', $csv);
    $clmCount = sizeof(fgetcsv(fopen($csvPath, "r"), 1000, ","));
    return array($csv,$clmCount);
}

function createInsertSql($csv, $tableName, $clmCount) {
    // csvを整形してSQL作成
    $cnt = 0;

    foreach ($csv as $clm) {
        $clmData = "";
        for ($i = 0; $i < $clmCount; $i ++) {
            $clmData .= implode(array("'", $clm[$i], "',"));
            if ($clmCount - 1 == $i) {
                $clmData = substr($clmData, 0, - 1);
            }
        }

        if (count($csv) - 1 > $cnt) {
            $columns[] = '(' . $clmData . '),';
        } else {
            $columns[] = '(' . $clmData . ')';
        }
        $cnt ++;
    }

    $insertSql = "";
    foreach ($columns as $record) {
        if ($insertSql == "") {
            $clmName = substr($record, 0, -1);
            $insertSql = "INSERT INTO " . $tableName . " VALUES ";
        } else {
            $insertSql .= $record."\r\n";
        }
    }
    $newFileName = date('Ymd_hi_(s)') . '_insertData.sql';
    // SQLファイルが無ければ作成(バックアップ)
    createSqlFile($newFileName);
    writeSqlToText($newFileName, $insertSql);

    return array($insertSql,$clmName);
}

function createSqlFile($fileName) {
    $backUpFileName = '/vagrant/oopRen/sql_backup/' . $fileName;
    if (!file_exists($fileName)) {
        touch($backUpFileName);
    } else {
        exit();
    }
    return $backUpFileName;
}

function writeSqlToText($fileName, $sqlText) {
    $backUpFileName = '/vagrant/oopRen/sql_backup/' . $fileName;
    if (file_exists($backUpFileName)) {
        $fp = fopen($backUpFileName, "w");
        fwrite($fp, $sqlText);
        fclose($fp);
    } else {
        echo "エラー:" . $backUpFileName . "が見つかりません";
        continue;
    }
}