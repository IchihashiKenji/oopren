<?php
require_once __DIR__.'/src/CreateSql.php';

$csvPath = __DIR__.'/../csv/D01.csv';
$outPath = __DIR__.'/../output_file/jsonTest.json';

$rtn = readCsvFile($csvPath);
$csvData = $rtn[0];
$clmCount = $rtn[1];

//TODO 計算量 o(n^2)→o(n)

if (file_exists($outPath)) {
    unlink($outPath);
}
touch($outPath);
for ($j = 1; $j < count($csvData); $j ++) {
    $fp = fopen($outPath, "a");
    $writeText = createJson($csvData, $clmCount, $j);
    fwrite($fp, $writeText);
    fclose($fp);
}
echo 'complete!!';
exit();



function createJson($csvData, $clmCount, $j) {
    $jsonRecord = '{[';
    for ($i = 0; $i < $clmCount; $i ++) {
        $jsonRecord .= '"' . $csvData[0][$i] . '":"' . $csvData[$j][$i] . '",';
        if ($clmCount - 1 == $i) {
            $jsonRecord = substr($jsonRecord, 0, - 1);
            $jsonRecord .= "]}".PHP_EOL;
        }
    }
    return $jsonRecord;
}

